%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!1011 &101100000
AvatarMask:
  m_ObjectHideFlags: 0
  m_PrefabParentObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: WYMask
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Bip001
    m_Weight: 0
  - m_Path: Bip001/B_Head
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/B_foot
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 L Thigh
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 L Thigh/Bip001 L Calf
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 L Thigh/Bip001 L Calf/Bip001 L Foot
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 R Thigh
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 R Thigh/Bip001 R Calf
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 R Thigh/Bip001 R Calf/Bip001 R Foot
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bone PelvisBackLeft01
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bone PelvisBackLeft01/Bone PelvisBackLeft02
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bone PelvisBackLeft01/Bone PelvisBackLeft02/Bone
      PelvisBackLeft03
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bone PelvisBackRight01
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bone PelvisBackRight01/Bone PelvisBackRight02
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bone PelvisBackRight01/Bone PelvisBackRight02/Bone
      PelvisBackRight03
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bone PelvisFront01
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bone PelvisFront01/Bone PelvisFront02
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bone PelvisFront01/Bone PelvisFront02/Bone PelvisFront03
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bone PelvisLeft01
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bone PelvisLeft01/Bone PelvisLeft02
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bone PelvisLeft01/Bone PelvisLeft02/Bone PelvisLeft03
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bone PelvisRight01
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bone PelvisRight01/Bone PelvisRight02
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bone PelvisRight01/Bone PelvisRight02/Bone PelvisRight03
    m_Weight: 0
  - m_Path: Bip001/Bip001 Spine
    m_Weight: 1
  - m_Path: Bip001/Bip001 Spine/Bip001 Spine1
    m_Weight: 1
  - m_Path: Bip001/Bip001 Spine/Bip001 Spine1/B_Back
    m_Weight: 1
  - m_Path: Bip001/Bip001 Spine/Bip001 Spine1/Bip001 L Clavicle
    m_Weight: 1
  - m_Path: Bip001/Bip001 Spine/Bip001 Spine1/Bip001 L Clavicle/Bip001 L UpperArm
    m_Weight: 1
  - m_Path: Bip001/Bip001 Spine/Bip001 Spine1/Bip001 L Clavicle/Bip001 L UpperArm/Bip001
      L Forearm
    m_Weight: 1
  - m_Path: Bip001/Bip001 Spine/Bip001 Spine1/Bip001 L Clavicle/Bip001 L UpperArm/Bip001
      L Forearm/Bip001 L Hand
    m_Weight: 1
  - m_Path: Bip001/Bip001 Spine/Bip001 Spine1/Bip001 L Clavicle/Bip001 L UpperArm/Bip001
      L Forearm/Bip001 L Hand/Bip001 L Finger0
    m_Weight: 1
  - m_Path: Bip001/Bip001 Spine/Bip001 Spine1/Bip001 L Clavicle/Bip001 L UpperArm/Bip001
      L Forearm/Bip001 L Hand/Bip001 L Finger0/Bip001 L Finger01
    m_Weight: 1
  - m_Path: Bip001/Bip001 Spine/Bip001 Spine1/Bip001 L Clavicle/Bip001 L UpperArm/Bip001
      L Forearm/Bip001 L Hand/Bone_LH
    m_Weight: 1
  - m_Path: Bip001/Bip001 Spine/Bip001 Spine1/Bip001 L Clavicle/Bip001 L UpperArm/Bip001
      L Forearm/Bip001 L Hand/Bone_LH/B_LH
    m_Weight: 1
  - m_Path: Bip001/Bip001 Spine/Bip001 Spine1/Bip001 L Clavicle/Bip001 L UpperArm/Bip001
      L Forearm/Bone ArmLeft01
    m_Weight: 1
  - m_Path: Bip001/Bip001 Spine/Bip001 Spine1/Bip001 L Clavicle/Bip001 L UpperArm/Bip001
      L Forearm/Bone ArmLeft01/Bone ArmLeft02
    m_Weight: 1
  - m_Path: Bip001/Bip001 Spine/Bip001 Spine1/Bip001 L Clavicle/Bip001 L UpperArm/Bip001
      L Forearm/Bone ArmLeft01/Bone ArmLeft02/Bone ArmLeft03
    m_Weight: 1
  - m_Path: Bip001/Bip001 Spine/Bip001 Spine1/Bip001 Neck
    m_Weight: 1
  - m_Path: Bip001/Bip001 Spine/Bip001 Spine1/Bip001 Neck/Bip001 Head
    m_Weight: 1
  - m_Path: Bip001/Bip001 Spine/Bip001 Spine1/Bip001 Neck/Bip001 Head/B_Face
    m_Weight: 1
  - m_Path: Bip001/Bip001 Spine/Bip001 Spine1/Bip001 Neck/Bip001 Head/Bone HairBackLow01
    m_Weight: 1
  - m_Path: Bip001/Bip001 Spine/Bip001 Spine1/Bip001 Neck/Bip001 Head/Bone HairBackLow01/Bone
      HairBackLow02
    m_Weight: 1
  - m_Path: Bip001/Bip001 Spine/Bip001 Spine1/Bip001 Neck/Bip001 Head/Bone HairBackLow01/Bone
      HairBackLow02/Bone HairBackLow03
    m_Weight: 1
  - m_Path: Bip001/Bip001 Spine/Bip001 Spine1/Bip001 Neck/Bip001 Head/Bone HairBackLow01/Bone
      HairBackLow02/Bone HairBackLow03/Bone HairBackLow04
    m_Weight: 1
  - m_Path: Bip001/Bip001 Spine/Bip001 Spine1/Bip001 Neck/Bip001 Head/Bone HairBackMid01
    m_Weight: 1
  - m_Path: Bip001/Bip001 Spine/Bip001 Spine1/Bip001 Neck/Bip001 Head/Bone HairBackMid01/Bone
      HairBackMid02
    m_Weight: 1
  - m_Path: Bip001/Bip001 Spine/Bip001 Spine1/Bip001 Neck/Bip001 Head/Bone HairBackMid01/Bone
      HairBackMid02/Bone HairBackMid03
    m_Weight: 1
  - m_Path: Bip001/Bip001 Spine/Bip001 Spine1/Bip001 Neck/Bip001 Head/Bone HeadBackLeft01
    m_Weight: 1
  - m_Path: Bip001/Bip001 Spine/Bip001 Spine1/Bip001 Neck/Bip001 Head/Bone HeadBackLeft01/Bone
      HeadBackLeft02
    m_Weight: 1
  - m_Path: Bip001/Bip001 Spine/Bip001 Spine1/Bip001 Neck/Bip001 Head/Bone HeadBackRight01
    m_Weight: 1
  - m_Path: Bip001/Bip001 Spine/Bip001 Spine1/Bip001 Neck/Bip001 Head/Bone HeadBackRight01/Bone
      HeadBackRight02
    m_Weight: 1
  - m_Path: Bip001/Bip001 Spine/Bip001 Spine1/Bip001 R Clavicle
    m_Weight: 1
  - m_Path: Bip001/Bip001 Spine/Bip001 Spine1/Bip001 R Clavicle/Bip001 R UpperArm
    m_Weight: 1
  - m_Path: Bip001/Bip001 Spine/Bip001 Spine1/Bip001 R Clavicle/Bip001 R UpperArm/Bip001
      R Forearm
    m_Weight: 1
  - m_Path: Bip001/Bip001 Spine/Bip001 Spine1/Bip001 R Clavicle/Bip001 R UpperArm/Bip001
      R Forearm/Bip001 R Hand
    m_Weight: 1
  - m_Path: Bip001/Bip001 Spine/Bip001 Spine1/Bip001 R Clavicle/Bip001 R UpperArm/Bip001
      R Forearm/Bip001 R Hand/Bip001 R Finger0
    m_Weight: 1
  - m_Path: Bip001/Bip001 Spine/Bip001 Spine1/Bip001 R Clavicle/Bip001 R UpperArm/Bip001
      R Forearm/Bip001 R Hand/Bip001 R Finger0/Bip001 R Finger01
    m_Weight: 1
  - m_Path: Bip001/Bip001 Spine/Bip001 Spine1/Bip001 R Clavicle/Bip001 R UpperArm/Bip001
      R Forearm/Bip001 R Hand/Bone_RH
    m_Weight: 1
  - m_Path: Bip001/Bip001 Spine/Bip001 Spine1/Bip001 R Clavicle/Bip001 R UpperArm/Bip001
      R Forearm/Bip001 R Hand/Bone_RH/B_RH
    m_Weight: 1
  - m_Path: Bip001/Bip001 Spine/Bip001 Spine1/Bip001 R Clavicle/Bip001 R UpperArm/Bip001
      R Forearm/Bone ArmRight01
    m_Weight: 1
  - m_Path: Bip001/Bip001 Spine/Bip001 Spine1/Bip001 R Clavicle/Bip001 R UpperArm/Bip001
      R Forearm/Bone ArmRight01/Bone ArmRight02
    m_Weight: 1
  - m_Path: Bip001/Bip001 Spine/Bip001 Spine1/Bip001 R Clavicle/Bip001 R UpperArm/Bip001
      R Forearm/Bone ArmRight01/Bone ArmRight02/Bone ArmRight03
    m_Weight: 1
  - m_Path: Bip001/Bip001 Spine/Bip001 Spine1/Bone_Breast_Left
    m_Weight: 1
  - m_Path: Bip001/Bip001 Spine/Bip001 Spine1/Bone_Breast_Right
    m_Weight: 1
  - m_Path: F1_WY001_body_H
    m_Weight: 0
