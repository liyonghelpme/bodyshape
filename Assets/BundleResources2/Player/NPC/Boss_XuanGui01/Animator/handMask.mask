%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!1011 &101100000
AvatarMask:
  m_ObjectHideFlags: 0
  m_PrefabParentObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: handMask
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Bip001
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 L Thigh
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 L Thigh/behit01
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 L Thigh/Bip001 L Calf
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 L Thigh/Bip001 L Calf/Bip001
      L Foot
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 R Thigh
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 R Thigh/behit02
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 R Thigh/Bip001 R Calf
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 R Thigh/Bip001 R Calf/Bip001
      R Foot
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001
      L Clavicle
    m_Weight: 1
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001
      L Clavicle/Bip001 L UpperArm
    m_Weight: 1
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001
      L Clavicle/Bip001 L UpperArm/behit01 1
    m_Weight: 1
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001
      L Clavicle/Bip001 L UpperArm/Bip001 L Forearm
    m_Weight: 1
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001
      L Clavicle/Bip001 L UpperArm/Bip001 L Forearm/Bip001 L Hand
    m_Weight: 1
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001
      Neck1
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001
      Neck1/Bip001 Neck2
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001
      Neck1/Bip001 Neck2/Bip001 Neck3
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001
      Neck1/Bip001 Neck2/Bip001 Neck3/Bip001 Neck4
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001
      Neck1/Bip001 Neck2/Bip001 Neck3/Bip001 Neck4/Bip001 Head
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001
      Neck1/Bip001 Neck2/Bip001 Neck3/Bip001 Neck4/Bip001 Head/Bone041
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001
      Neck1/Bip001 Neck2/Bip001 Neck3/Bip001 Neck4/Bip001 Head/Bone041/Bone042
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001
      Neck1/Bip001 Neck2/Bip001 Neck3/Bip001 Neck4/Bip001 Head/Bone045
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001
      Neck1/Bip001 Neck2/Bip001 Neck3/Bip001 Neck4/Bip001 Head/Bone045/Bone046
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001
      Neck1/Bip001 Neck2/Bip001 Neck3/Bip001 Neck4/Bip001 Head/Bone045/Bone046/Bone047
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001
      Neck1/Bip001 Neck2/Bip001 Neck3/Bip001 Neck4/Bip001 Head/Bone045/Bone046/Bone047/Bone048
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001
      Neck1/Bip001 Neck2/Bip001 Neck3/Bip001 Neck4/Bip001 Head/Bone045/Bone046/Bone047/Bone048/Bone049
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001
      Neck1/Bip001 Neck2/Bip001 Neck3/Bip001 Neck4/Bip001 Head/Bone045/Bone046/Bone047/Bone048/Bone049/Bone050
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001
      Neck1/Bip001 Neck2/Bip001 Neck3/Bip001 Neck4/Bip001 Head/Bone045/Bone046/Bone047/Bone048/Bone049/Bone050/Bone051
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001
      Neck1/Bip001 Neck2/Bip001 Neck3/Bip001 Neck4/Bip001 Head/Bone045/Bone046/Bone047/Bone048/Bone049/Bone050/Bone051/Bone052
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001
      Neck1/Bip001 Neck2/Bip001 Neck3/Bip001 Neck4/Bip001 Head/Bone045/Bone046/Bone047/Bone048/Bone049/Bone050/Bone051/Bone052/Bone053
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001
      Neck1/Bip001 Neck2/Bip001 Neck3/Bip001 Neck4/Bip001 Head/Bone045(mirrored)
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001
      Neck1/Bip001 Neck2/Bip001 Neck3/Bip001 Neck4/Bip001 Head/Bone045(mirrored)/Bone046(mirrored)
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001
      Neck1/Bip001 Neck2/Bip001 Neck3/Bip001 Neck4/Bip001 Head/Bone045(mirrored)/Bone046(mirrored)/Bone047(mirrored)
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001
      Neck1/Bip001 Neck2/Bip001 Neck3/Bip001 Neck4/Bip001 Head/Bone045(mirrored)/Bone046(mirrored)/Bone047(mirrored)/Bone048(mirrored)
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001
      Neck1/Bip001 Neck2/Bip001 Neck3/Bip001 Neck4/Bip001 Head/Bone045(mirrored)/Bone046(mirrored)/Bone047(mirrored)/Bone048(mirrored)/Bone049(mirrored)
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001
      Neck1/Bip001 Neck2/Bip001 Neck3/Bip001 Neck4/Bip001 Head/Bone045(mirrored)/Bone046(mirrored)/Bone047(mirrored)/Bone048(mirrored)/Bone049(mirrored)/Bone050(mirrored)
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001
      Neck1/Bip001 Neck2/Bip001 Neck3/Bip001 Neck4/Bip001 Head/Bone045(mirrored)/Bone046(mirrored)/Bone047(mirrored)/Bone048(mirrored)/Bone049(mirrored)/Bone050(mirrored)/Bone051(mirrored)
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001
      Neck1/Bip001 Neck2/Bip001 Neck3/Bip001 Neck4/Bip001 Head/Bone045(mirrored)/Bone046(mirrored)/Bone047(mirrored)/Bone048(mirrored)/Bone049(mirrored)/Bone050(mirrored)/Bone051(mirrored)/Bone052(mirrored)
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001
      Neck1/Bip001 Neck2/Bip001 Neck3/Bip001 Neck4/Bip001 Head/Bone045(mirrored)/Bone046(mirrored)/Bone047(mirrored)/Bone048(mirrored)/Bone049(mirrored)/Bone050(mirrored)/Bone051(mirrored)/Bone052(mirrored)/Bone053(mirrored)
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001
      Neck1/Bip001 Neck2/Bip001 Neck3/Bip001 Neck4/Bip001 Head/flyhit02
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001
      R Clavicle
    m_Weight: 1
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001
      R Clavicle/Bip001 R UpperArm
    m_Weight: 1
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001
      R Clavicle/Bip001 R UpperArm/behit02 1
    m_Weight: 1
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001
      R Clavicle/Bip001 R UpperArm/Bip001 R Forearm
    m_Weight: 1
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001
      R Clavicle/Bip001 R UpperArm/Bip001 R Forearm/Bip001 R Hand
    m_Weight: 1
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/touL_Bone01
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/touL_Bone01/touL_Bone02
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/touL_Bone01/touL_Bone02/touL_Bone03
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/touL_Bone01/touL_Bone02/touL_Bone03/touL_Bone04
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/touL_Bone01/touL_Bone02/touL_Bone03/touL_Bone04/touL_Bone05
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/touL_Bone01/touL_Bone02/touL_Bone03/touL_Bone04/touL_Bone05/touL_Bone06
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/touL_Bone01/touL_Bone02/touL_Bone03/touL_Bone04/touL_Bone05/touL_Bone06/flyhit03
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/touL_Bone01/touL_Bone02/touL_Bone03/touL_Bone04/touL_Bone05/touL_Bone06/touL_Bone07
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/touL_Bone01/touL_Bone02/touL_Bone03/touL_Bone04/touL_Bone05/touL_Bone06/touL_Bone07/Bone044(mirrored)
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/touL_Bone01/touL_Bone02/touL_Bone03/touL_Bone04/touL_Bone05/touL_Bone06/touL_Bone08
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/touR_Bone01
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/touR_Bone01/touR_Bone02
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/touR_Bone01/touR_Bone02/touR_Bone03
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/touR_Bone01/touR_Bone02/touR_Bone03/touR_Bone04
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/touR_Bone01/touR_Bone02/touR_Bone03/touR_Bone04/touR_Bone05
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/touR_Bone01/touR_Bone02/touR_Bone03/touR_Bone04/touR_Bone05/touR_Bone06
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/touR_Bone01/touR_Bone02/touR_Bone03/touR_Bone04/touR_Bone05/touR_Bone06/flyhit01
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/touR_Bone01/touR_Bone02/touR_Bone03/touR_Bone04/touR_Bone05/touR_Bone06/touR_Bone06
      1
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/touR_Bone01/touR_Bone02/touR_Bone03/touR_Bone04/touR_Bone05/touR_Bone06/touR_Bone06
      1/Bone044
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/touR_Bone01/touR_Bone02/touR_Bone03/touR_Bone04/touR_Bone05/touR_Bone06/touR_Bone07
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/Bip001 Spine/fall01
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/weiba_Bone01
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/weiba_Bone01/weiba_Bone02
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/weiba_Bone01/weiba_Bone02/weiba_Bone03
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/weiba_Bone01/weiba_Bone02/weiba_Bone03/weiba_Bone04
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/weiba_Bone01/weiba_Bone02/weiba_Bone03/weiba_Bone04/weiba_Bone05
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/weiba_Bone01/weiba_Bone02/weiba_Bone03/weiba_Bone04/weiba_Bone05/weiba_Bone06
    m_Weight: 0
  - m_Path: Bip001/Bip001 Pelvis/weiba_Bone01/weiba_Bone02/weiba_Bone03/weiba_Bone04/weiba_Bone05/weiba_Bone06/weiba_Bone07
    m_Weight: 0
  - m_Path: NPC_Boss_XuanGui01
    m_Weight: 0
