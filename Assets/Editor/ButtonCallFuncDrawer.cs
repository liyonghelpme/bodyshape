﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Reflection;


[CustomPropertyDrawer(typeof(ButtonCallFunc))]
public class ButtonCallFuncDrawer : PropertyDrawer
{
    const int butHeight = 16;
    const int pad = 5;

    ButtonCallFunc cutsceneAttribute
    {
        get
        {
            return (ButtonCallFunc)attribute;
        }
    }
    public override float GetPropertyHeight(SerializedProperty prop, GUIContent label)
    {
        return base.GetPropertyHeight(prop, label) + pad;
    }
    public override void OnGUI(Rect position, SerializedProperty prop, GUIContent label)
    {
        //Rect textFieldPosition = position;
        //textFieldPosition.height = butHeight;
        //cutsceneAttribute.dialog = EditorGUI.TextField (textFieldPosition, new GUIContent(label), cutsceneAttribute.dialog);

        Rect butPosition = position;
        //butPosition.y += butHeight;
        //butPosition.height = butHeight;

        if (GUI.Button(butPosition, prop.propertyPath))
        {
            var cs = prop.serializedObject.targetObject;
            MethodInfo mi = cs.GetType().GetMethod(prop.propertyPath + "Method");
            //object[] paramsArr = new object[]{cutsceneAttribute.dialog};
            mi.Invoke(cs, null);
        }
    }
}


[CustomPropertyDrawer(typeof(ListCallFunc))]
public class ListCallFuncDrawer : PropertyDrawer
{
    const int butHeight = 16;
    const int pad = 5;

    ListCallFunc cutsceneAttribute
    {
        get
        {
            return (ListCallFunc)attribute;
        }
    }
    public override float GetPropertyHeight(SerializedProperty prop, GUIContent label)
    {
        return base.GetPropertyHeight(prop, label) + pad;
    }
    public override void OnGUI(Rect position, SerializedProperty prop, GUIContent label)
    {
        var cs = prop.serializedObject.targetObject;
        //var attriList = (IList)cs.GetType().GetField(prop.propertyPath);

        //Debug.Log("PropertyName:"+prop.propertyPath);
        var splitArr = prop.propertyPath.Split('.');
        var attrName = splitArr[0];
        var dataId = splitArr[2].Replace("data", "").Replace("[", "").Replace("]", "");
        var did = System.Convert.ToInt32(dataId);

        var attriList = (cs.GetType().GetField(attrName));
        var obj = (IList)attriList.GetValue(cs);

        var dt = obj[did];
        //Debug.Log("AttriList:" + attriList.ToString()+":"+attriList.GetType()+":"+obj+":"+obj.GetType());

        var butPos = position;
        if (GUI.Button(butPos, dt.ToString().Split('(')[0]))
        {
            var mi = cs.GetType().GetMethod(attrName + "Method");
            mi.Invoke(cs, new object[]{did});
        }

        /*
        var c = 0;
        foreach (var a in attriList)
        {
            var butPos = position;
            butPos.y += butHeight * c;
            if (GUI.Button(butPos, a.ToString()))
            {
                var mi = cs.GetType().GetMethod(prop.propertyPath + "Method");
                var ind = c;
                mi.Invoke(cs, new object[]{ind});
            }
            c++;
        }
        */

        /*
        Rect butPosition = position;

        if (GUI.Button(butPosition, prop.propertyPath))
        {
            var cs = prop.serializedObject.targetObject;
            MethodInfo mi = cs.GetType().GetMethod(prop.propertyPath + "Method");
            //object[] paramsArr = new object[]{cutsceneAttribute.dialog};
            mi.Invoke(cs, null);
        }
        */
    }
}