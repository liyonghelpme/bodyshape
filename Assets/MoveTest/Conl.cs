﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Conl : MonoBehaviour {

    public Animator mainAnimator;
    public GameObject moveobj;
    public GameObject faceobj;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        mainAnimator.SetBool("Move", true);
        transform.forward = (faceobj.transform.position - transform.position).normalized;

        var npcForward = transform.forward;
        var movedir = (moveobj.transform.position - transform.position).normalized;
        var ang = Vector3.Angle(npcForward, movedir);
        float y = Mathf.Lerp(0.5f, -0.5f, ang / 180f);
        var crossv = Vector3.Cross(npcForward, movedir);
        float x = Mathf.Lerp(-0.5f, 0.5f, (crossv.y + 1f) / 2f);
        /*
        if (!IsMoving)
        {
            x = 0f;
            y = 0f;
        }
        */
        mainAnimator.SetFloat("MoveDirX", x);
        mainAnimator.SetFloat("MoveDirY", y);
    }


   
}
