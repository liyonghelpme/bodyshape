﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyEdit : MonoBehaviour {
    public float neckLen = 0;
    public float headSize = 0;
    public float hipSize = 0;
    public float shoulderWidth = 0;
    public float bodyHeight = 0;
    public float legLen = 0;
    public float lowLegLen = 0;
    public float feetLen = 0;
    public float upArmLen = 0;
    public float lowArmLen = 0;
    public float breastSize = 0;



    public GameObject model;
    public GameObject viewModel;
    private Transform[] allChild;
    private Transform[] baseChild;

    void Awake()
    {
    }

    public Transform selectObj;

    public Transform neck;
    public Transform neckBase;

    public Transform hipA, hipB;
    public Transform hipABase, hipBBase;

    public Transform shoulderA, shoulderB;
    public Transform shoulderABase, shoulderBBase;

    public Transform bodyH;
    public Transform bodyHBase;

    public Transform upLegA, upLegB;
    public Transform upLegBaseA, upLegBaseB;

    public Transform lowLegA,lowLegB;
    public Transform lowLegBaseA, lowLegBaseB;

    public Transform feetA,feetB;
    public Transform feetBaseA, feetBaseB;

    public Transform upArmA, upArmB;
    public Transform upArmBaseA, upArmBaseB;

    public Transform lowArmA, lowArmB;
    public Transform lowArmBaseA, lowArmBaseB;

    public Transform breastL, breastR;
    public Transform breastLBase, breastRBase;

    [ButtonCallFunc()]
    public bool LoadModel;
    public void LoadModelMethod()
    {
        if(viewModel != null)
        {
            GameObject.DestroyImmediate(viewModel);
        }
        viewModel = GameObject.Instantiate(model) as GameObject;
        viewModel.transform.position = Vector3.zero;
        allChild = viewModel.GetComponentsInChildren<Transform>();
        baseChild = model.GetComponentsInChildren<Transform>();


        var neckBone = "Bip001 Head";
        neck = FindChild(neckBone);
        neckBase = FindBase(neckBone);

        var hipBone = "Bone005";
        hipA = FindChild(hipBone);
        hipABase = FindBase(hipBone);
        hipBone = "Bone005(mirrored)";
        hipB = FindChild(hipBone);
        hipBBase = FindBase(hipBone);

        var shoulderBone = "Bip001 L Clavicle";
        shoulderA = FindChild(shoulderBone);
        shoulderABase = FindBase(shoulderBone);

        shoulderBone = "Bip001 R Clavicle";
        shoulderB = FindChild(shoulderBone);
        shoulderBBase = FindBase(shoulderBone);

        var bodyBone = "Bip001 Spine1";
        bodyH = FindChild(bodyBone);
        bodyHBase = FindBase(bodyBone);

        var upLegName = "Bip001 L Calf";
        upLegA = FindChild(upLegName);
        upLegBaseA = FindBase(upLegName);
        upLegName = "Bip001 R Calf";
        upLegB = FindChild(upLegName);
        upLegBaseB = FindBase(upLegName);

        var lowLegName = "Bip001 R Foot";
        lowLegA = FindChild(lowLegName);
        lowLegBaseA = FindBase(lowLegName);
        lowLegName = "Bip001 L Foot";
        lowLegB = FindChild(lowLegName);
        lowLegBaseB = FindBase(lowLegName);

        var feetName = "Bip001 R Toe0";
        feetA = FindChild(feetName);
        feetBaseA = FindBase(feetName);
        feetName = "Bip001 L Toe0";
        feetB = FindChild(feetName);
        feetBaseB = FindBase(feetName);

        var upArmName = "Bip001 L Forearm";
        upArmA = FindChild(upArmName);
        upArmBaseA = FindBase(upArmName);
        upArmName = "Bip001 R Forearm";
        upArmB = FindChild(upArmName);
        upArmBaseB = FindBase(upArmName);

        var lowArmName = "Bip001 L Hand";
        lowArmA = FindChild(lowArmName);
        lowArmBaseA = FindBase(lowArmName);
        lowArmName = "Bip001 R Hand";
        lowArmB = FindChild(lowArmName);
        lowArmBaseB = FindBase(lowArmName);


        var breastName = "Bone_Breast_Left";
        breastL = FindChild(breastName);
        breastLBase = FindBase(breastName);

        breastName = "Bone_Breast_Right";
        breastR = FindChild(breastName);
        breastRBase = FindBase(breastName);


    }

    [ButtonCallFunc()]
    public bool UpdateConfig;
    public void UpdateConfigMethod()
    {
        neck.localPosition = neckBase.localPosition + new Vector3(-neckLen, 0, 0);
        neck.localScale = neckBase.localScale + Vector3.one * headSize;

        if (hipA != null)
        {
            hipA.localPosition = hipABase.localPosition + hipA.localRotation * new Vector3(0, 0, -hipSize);
            hipB.localPosition = hipBBase.localPosition + hipB.localRotation * new Vector3(0, 0, -hipSize);
        }

        shoulderA.localPosition = shoulderABase.localPosition + shoulderA.localRotation * new Vector3(-shoulderWidth, 0, 0);
        shoulderB.localPosition = shoulderBBase.localPosition + shoulderB.localRotation * new Vector3(-shoulderWidth, 0, 0);
        bodyH.localPosition = bodyHBase.localPosition + bodyHBase.localRotation * new Vector3(-bodyHeight, 0, 0);

        upLegA.localPosition = upLegBaseA.localPosition + upLegBaseA.localRotation * new Vector3(-legLen, 0, 0);
        upLegB.localPosition = upLegBaseB.localPosition + upLegBaseB.localRotation * new Vector3(-legLen, 0, 0);

        lowLegA.localPosition = lowLegBaseA.localPosition + lowLegBaseA.localRotation * new Vector3(-lowLegLen, 0, 0);
        lowLegB.localPosition = lowLegBaseB.localPosition + lowLegBaseB.localRotation * new Vector3(-lowLegLen, 0, 0);

        if (feetA != null)
        {
            feetA.localPosition = feetBaseA.localPosition + feetBaseA.localRotation * new Vector3(-feetLen, 0, 0);
            feetB.localPosition = feetBaseB.localPosition + feetBaseB.localRotation * new Vector3(-feetLen, 0, 0);
        }

        upArmA.localPosition = upArmBaseA.localPosition + upArmBaseA.localRotation * new Vector3(-upArmLen, 0, 0);
        upArmB.localPosition = upArmBaseB.localPosition + upArmBaseB.localRotation * new Vector3(-upArmLen, 0, 0);

        lowArmA.localPosition = lowArmBaseA.localPosition + lowArmBaseA.localRotation * new Vector3(-lowArmLen, 0, 0);
        lowArmB.localPosition = lowArmBaseB.localPosition + lowArmBaseB.localRotation * new Vector3(-lowArmLen, 0, 0);

        breastL.localPosition = breastL.localPosition + breastLBase.localRotation * new Vector3(-breastSize, 0, 0);
        breastR.localPosition = breastR.localPosition + breastRBase.localRotation * new Vector3(-breastSize, 0, 0);
    }

    [ButtonCallFunc()]
    public bool selectNeck;
    public void selectNeckMethod()
    {
        var neckBone = "Bip001 Head";
        var neck = FindChild(neckBone);
        selectObj = neck;
    }

    [ButtonCallFunc()]
    public bool SetConfigToViewModel;
    public void SetConfigToViewModelMethod()
    {
        var vm = viewModel.GetComponent<ViewModel>();
        if(vm == null)
        {
            vm = viewModel.AddComponent<ViewModel>();
        }
        vm.SetConfig(this);
    }

    public void ExportConfigure()
    {

    }
    public void LoadConfigure()
    {

    }


    private Transform FindChild(string name)
    {
        foreach(var c in allChild)
        {
            if(c.name == name)
            {
                return c;
            }
        }
        return null;
    }
    private Transform FindBase(string name)
    {
        foreach(var c in baseChild)
        {
            if(c.name == name)
            {
                return c;
            }
        }
        return null;
    }
    
}
