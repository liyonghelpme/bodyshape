﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
public class BodyShapeEdtor : EditorWindow {
	[MenuItem("BodyShape/Open")]
	public static void OpenBodyShape()
	{
		BodyShapeEdtor window = (BodyShapeEdtor)EditorWindow.GetWindow(typeof(BodyShapeEdtor));
		window.Show();
	}
	public class BakeData
	{
		public string Name;
		public float Value;
		public Dictionary<string, Vector3[]> ScaleData = new Dictionary<string, Vector3[]>();
		public Vector3 GetScale(string name)
		{
			if (!ScaleData.ContainsKey (name))
				return Vector3.one;
			return Vector3.Lerp (ScaleData [name] [0], ScaleData [name] [1], Value);
		}

	}
	public Transform Root;
	Dictionary<Transform, Vector3> m_mapBaseScale = new Dictionary<Transform, Vector3>();
	Dictionary<string, Transform> m_mapBips = new Dictionary<string, Transform>();
	Dictionary<Transform, Transform> m_mapBaseParent = new Dictionary<Transform, Transform>();
	List<BakeData> m_listData = new List<BakeData>();
	bool bView =false;
//	Dictionary<string, BakeData> m_mapData = new Dictionary<string, BakeData>();
	void OnGUI()
	{
		Root = (Transform)EditorGUILayout.ObjectField (Root, typeof(Transform), true);
		if (Root != null) {
			if (GUILayout.Button ("BakeBase")) {
				m_mapBaseScale.Clear ();
				m_mapBips.Clear ();
				m_mapBaseParent.Clear ();
				foreach (var t in Root.GetComponentsInChildren<Transform>()) {
					if (t.name.StartsWith ("Bip")) {
						m_mapBaseScale.Add (t, t.localScale);
						m_mapBips.Add (t.name, t);
						m_mapBaseParent.Add (t, t.parent);
					}
				}
			}
		}
		if (m_mapBaseScale.Count > 0) {
			if (GUILayout.Button ("RevertBase")) {
				foreach (var data in m_mapBaseScale) {
					data.Key.localScale = data.Value;
				}
			}
			if (GUILayout.Button ("BombParent")) {
				foreach (var data in m_mapBips) {
					data.Value.parent = null;
				}
			}
			if (GUILayout.Button ("RevertParent")) {
				foreach (var data in m_mapBips) {
					data.Value.parent = m_mapBaseParent[data.Value];
				}
			}
			bView = EditorGUILayout.Toggle ("View", bView);
			if (GUILayout.Button ("AddNew")) {
				m_listData.Add (new BakeData());
			}
			for (int i = 0; i < m_listData.Count; i++) {
				m_listData [i].Name = EditorGUILayout.TextField("Name",m_listData [i].Name);
				GUILayout.BeginHorizontal ();
				EditorGUILayout.LabelField ("Min", m_listData [i].ScaleData.Count.ToString());
				if (GUILayout.Button ("BakeMin")) {
					foreach (var t in Root.GetComponentsInChildren<Transform>()) {
						if (t.name.StartsWith ("Bip")) {
							if (t.localScale != m_mapBaseScale [t]) {
								var s = t.localScale;
								s.x /= m_mapBaseScale [t].x;
								s.y /= m_mapBaseScale [t].y;
								s.z /= m_mapBaseScale [t].z;
								if (!m_listData [i].ScaleData.ContainsKey (t.name))
									m_listData [i].ScaleData.Add (t.name, new Vector3[]{Vector3.one, Vector3.one});
								m_listData [i].ScaleData [t.name] [0] = s;
							}
						}
					}
				}
				GUILayout.EndHorizontal ();
				GUILayout.BeginHorizontal ();
				EditorGUILayout.LabelField ("Max", m_listData [i].ScaleData.Count.ToString());
				if (GUILayout.Button ("BakeMax")) {
					foreach (var t in Root.GetComponentsInChildren<Transform>()) {
						if (t.name.StartsWith ("Bip")) {
							if (t.localScale != m_mapBaseScale [t]) {
								var s = t.localScale;
								s.x /= m_mapBaseScale [t].x;
								s.y /= m_mapBaseScale [t].y;
								s.z /= m_mapBaseScale [t].z;
								if (!m_listData [i].ScaleData.ContainsKey (t.name))
									m_listData [i].ScaleData.Add (t.name, new Vector3[]{Vector3.one, Vector3.one});
								m_listData [i].ScaleData [t.name] [1] = s;
							}
						}
					}
				}
				GUILayout.EndHorizontal ();
				if (m_listData [i].ScaleData.Count > 0) {
					m_listData [i].Value = GUILayout.HorizontalSlider (m_listData [i].Value, 0f, 1f);
				}
				if (GUILayout.Button ("Del")) {
					m_listData.Remove (m_listData [i]);
					break;
				}
				GUILayout.Space (5);
			}
		}

		if (!bView) {
			return;
		}
		Dictionary<string, Vector3> Scale = new Dictionary<string, Vector3> ();
		for (int i = 0; i < m_listData.Count; i++) {
			var data = m_listData [i];
			if (m_listData [i].ScaleData.Count == 0)
				continue;
			foreach(var sdata in m_listData [i].ScaleData)
			{
				if (!Scale.ContainsKey (sdata.Key))
					Scale [sdata.Key] = data.GetScale (sdata.Key);
				else
					Scale [sdata.Key].Scale(data.GetScale (sdata.Key));
			}
		}
		foreach (var sdata in Scale) {
			m_mapBips [sdata.Key].transform.localScale = sdata.Value;
		}

	}

	void LateUpdate()
	{
		

	}

}
