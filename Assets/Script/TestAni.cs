﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestAni : MonoBehaviour {

    [ButtonCallFunc()]
    public bool DoHand;
    public void DoHandMethod()
    {
        var ani = GetComponent<Animator>();
        ani.Play("at01_Hand", 1);
    }

    [ButtonCallFunc()]
    public bool Stop;
    public void StopMethod()
    {
        var ani = GetComponent<Animator>();
        ani.Play("st", 1);
    }
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
