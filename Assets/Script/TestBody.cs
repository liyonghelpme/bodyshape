﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestBody : MonoBehaviour {
    public float moveX = 0;
    public float moveY = 0.5f;

    public bool Walk;
    public bool Attack;
    public bool Attack2;

    public bool SimpleAttack;
    public bool SimpleAttack2;

	// Use this for initialization
	void Start () {
		
	}
    private bool lastWalk = false;
    private bool lastAttack = false;
    private bool lastSimpleAttack = false;
    private bool lastAttack2 = false;
    private bool lastSimpleAttack2 = false;

	// Update is called once per frame
	void Update () {
        /*
        GetComponent<Animator>().SetBool("walk", Walk);
        GetComponent<Animator>().SetBool("attack", Attack);
        if(Attack)
        {
            GetComponent<Animator>().SetLayerWeight(1, 1);
        }else
        {
            GetComponent<Animator>().SetLayerWeight(1, 0);
        }
        */
        GetComponent<Animator>().SetFloat("MoveX", moveX);
        GetComponent<Animator>().SetFloat("MoveY", moveY);
         
        if (Walk != lastWalk)
        {
            lastWalk = Walk;
            if (Walk)
            {
                GetComponent<Animator>().CrossFade("run", 0.1f, 0);
            }
            else
            {
                GetComponent<Animator>().CrossFade("st", 0.1f, 0);
            }
        }
        if (lastAttack != Attack)
        {
            lastAttack = Attack;
            if (Attack)
            {
                GetComponent<Animator>().CrossFade("at01", 0.1f, 1);
                GetComponent<Animator>().SetLayerWeight(1, 1);
            }
            else
            {
                GetComponent<Animator>().CrossFade("st", 0.1f, 1);
                GetComponent<Animator>().SetLayerWeight(1, 0);
            }
        }

        if (lastAttack2 != Attack2)
        {
            lastAttack2 = Attack2;
            if (Attack2)
            {
                GetComponent<Animator>().CrossFade("at02", 0.1f, 1);
                GetComponent<Animator>().SetLayerWeight(1, 1);
            }
            else
            {
                GetComponent<Animator>().CrossFade("st", 0.1f, 1);
                GetComponent<Animator>().SetLayerWeight(1, 0);
            }
        }

        if(lastSimpleAttack != SimpleAttack)
        {
            lastSimpleAttack = SimpleAttack;

            GetComponent<Animator>().SetLayerWeight(1, 0);
            if (lastSimpleAttack)
            {
                GetComponent<Animator>().CrossFade("at01", 0.1f, 0);
            }
            else
            {
                GetComponent<Animator>().CrossFade("st", 0.1f, 0);
            }
        }

        if (lastSimpleAttack2 != SimpleAttack2)
        {
            lastSimpleAttack2 = SimpleAttack2;

            GetComponent<Animator>().SetLayerWeight(1, 0);
            if (lastSimpleAttack2)
            {
                GetComponent<Animator>().CrossFade("at02", 0.1f, 0);
            }
            else
            {
                GetComponent<Animator>().CrossFade("st", 0.1f, 0);
            }
        }
    }
}
