﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViewModel : MonoBehaviour {
    private BodyEdit bodyEdit;
    public void SetConfig(BodyEdit be)
    {
        bodyEdit = be;
    }

    private void Awake()
    {
    }
    private void LateUpdate()
    {
        bodyEdit.neck.transform.localPosition = bodyEdit.neck.transform.localPosition + new Vector3(-bodyEdit.neckLen, 0, 0);
        bodyEdit.neck.transform.localScale = bodyEdit.neck.transform.localScale + Vector3.one * bodyEdit.headSize;

        if (bodyEdit.hipA != null)
        {
            bodyEdit.hipA.transform.localPosition = bodyEdit.hipA.transform.localPosition + bodyEdit.hipA.localRotation * new Vector3(0, 0, -bodyEdit.hipSize);
            bodyEdit.hipB.transform.localPosition = bodyEdit.hipB.transform.localPosition + bodyEdit.hipB.localRotation * new Vector3(0, 0, -bodyEdit.hipSize);
        }

        bodyEdit.shoulderA.localPosition = bodyEdit.shoulderA.localPosition + bodyEdit.shoulderA.localRotation * new Vector3(-bodyEdit.shoulderWidth, 0, 0);
        bodyEdit.shoulderB.localPosition = bodyEdit.shoulderB.localPosition + bodyEdit.shoulderB.localRotation * new Vector3(-bodyEdit.shoulderWidth, 0, 0);

        bodyEdit.bodyH.localPosition = bodyEdit.bodyH.localPosition + bodyEdit.bodyH.localRotation * new Vector3(-bodyEdit.bodyHeight, 0, 0);


        bodyEdit.upLegA.localPosition = bodyEdit.upLegA.localPosition + bodyEdit.upLegA.localRotation * new Vector3(-bodyEdit.legLen, 0, 0);
        bodyEdit.upLegB.localPosition = bodyEdit.upLegB.localPosition + bodyEdit.upLegB.localRotation * new Vector3(-bodyEdit.legLen, 0, 0);

        bodyEdit.lowLegA.localPosition = bodyEdit.lowLegA.localPosition + bodyEdit.lowLegA.localRotation * new Vector3(-bodyEdit.lowLegLen, 0, 0);
        bodyEdit.lowLegB.localPosition = bodyEdit.lowLegB.localPosition + bodyEdit.lowLegB.localRotation * new Vector3(-bodyEdit.lowLegLen, 0, 0);

        if (bodyEdit.feetA != null)
        {
            bodyEdit.feetA.localPosition = bodyEdit.feetA.localPosition + bodyEdit.feetA.localRotation * new Vector3(-bodyEdit.feetLen, 0, 0);
            bodyEdit.feetB.localPosition = bodyEdit.feetB.localPosition + bodyEdit.feetB.localRotation * new Vector3(-bodyEdit.feetLen, 0, 0);
        }

        bodyEdit.upArmA.localPosition = bodyEdit.upArmA.localPosition + bodyEdit.upArmA.localRotation * new Vector3(-bodyEdit.upArmLen, 0, 0);
        bodyEdit.upArmB.localPosition = bodyEdit.upArmB.localPosition + bodyEdit.upArmB.localRotation * new Vector3(-bodyEdit.upArmLen, 0, 0);

        bodyEdit.lowArmA.localPosition = bodyEdit.lowArmA.localPosition + bodyEdit.lowArmA.localRotation * new Vector3(-bodyEdit.lowArmLen, 0, 0);
        bodyEdit.lowArmB.localPosition = bodyEdit.lowArmB.localPosition + bodyEdit.lowArmB.localRotation * new Vector3(-bodyEdit.lowArmLen, 0, 0);


        bodyEdit.breastL.localPosition = bodyEdit.breastL.localPosition + bodyEdit.breastL.localRotation * new Vector3(-bodyEdit.breastSize, 0, 0);
        bodyEdit.breastR.localPosition = bodyEdit.breastR.localPosition + bodyEdit.breastR.localRotation * new Vector3(-bodyEdit.breastSize, 0, 0);
    }
}
